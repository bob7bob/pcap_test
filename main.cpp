#include <ctype.h>

#include <pcap.h>
#include <stdio.h>
//#include <net/ethernet.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>

#include <arpa/inet.h>

void usage() {
  printf("syntax: pcap_test <interface>\n");
  printf("sample: pcap_test wlan0\n");
}


void PrintData(char *data, const size_t kDataLen)
{
  printf("[+]       Data\n");
  printf("          ");
  for(int i = 0; i < kDataLen; i++)
  {
    printf("%02x ", i);
  }
  printf("  ");
  for(int i = 0; i < kDataLen; i++)
  {
    printf("%1x", (unsigned char)i);
  }  
  printf("\n          ");

  for(int i = 0; i < kDataLen; i++)
  {
    printf("%02x ", (unsigned char)data[i]);
  }
  printf("  ");
  for(int i = 0; i < kDataLen; i++)
  {
    if(isprint(data[i]))
      printf("%c", data[i]);
    else
      printf(".");
  }
  printf("\n");
}

void PrintTcp(struct tcphdr *tcp)
{
  printf("[+]     TCP\n");
  printf("        Source      Port : %u\n", htons(tcp->th_sport)); //tcp+0x0 packet+34
  printf("        Destination Port : %u\n", htons(tcp->th_dport)); //tcp+0x2 packet+36
}

void PrintEthernet(struct ether_header* eth) //ether = packet + 0
{
  printf("[+] Ethernet\n");
  printf("    Destination Mac Address : %s\n", ether_ntoa((struct ether_addr*)eth->ether_dhost)); //eth+0x0
  printf("    Source      Mac Address : %s\n", ether_ntoa((struct ether_addr*)eth->ether_shost)); //eth+0x6
}

void PrintIpv4(struct ip *ip_header) //ip = ether +0xe
{
  printf("[+]   IPv4\n");
  printf("      Source      IP Address : %s\n", inet_ntoa(ip_header->ip_src)); //ip+0xc , packet+26
  printf("      Destination IP Address : %s\n", inet_ntoa(ip_header->ip_dst)); //ip+0x10, packet+30
  
}


void PrintIpv6(struct ip6_hdr *ip6) //ip = ether +0xe
{
  char ip_src[INET6_ADDRSTRLEN] = {0};
  char ip_dst[INET6_ADDRSTRLEN] = {0};

  printf("[+]   IPv6\n");
  inet_ntop(AF_INET6, &ip6->ip6_src, ip_src, sizeof(ip_src));//ip+0x8, packet+22
  inet_ntop(AF_INET6, &ip6->ip6_dst, ip_dst, sizeof(ip_dst));//ip+0x18 packet+38
  
  printf("      Source      IP Address : %s\n", ip_src); 
  printf("      Destination IP Address : %s\n", ip_dst); //ip+0x10 
}

void PrintPacket(const u_char* packet)
{
  struct ether_header* eth = (ether_header*)packet;
  PrintEthernet(eth);
  u_short ether_type = ntohs(eth->ether_type); //ether+0xC
  if(ether_type == ETHERTYPE_IP || ether_type == ETHERTYPE_IPV6) 
  {
    struct ip *ip_header = (struct ip*)(eth+1); //ether+14
    struct tcphdr *tcp;
    char ip_version = ip_header->ip_v;
    if(ip_version == 4)
    {
      PrintIpv4(ip_header);
    
      if(ip_header->ip_p == IPPROTO_TCP) //ip+9, packet + 23
      {
        size_t ip_header_len = ip_header->ip_hl * 4;
        tcp = (tcphdr*)((char*)ip_header+ip_header_len); //ip+20, packet + 34 
        PrintTcp(tcp);
        char *data = (char*)(tcp+1); //tcp+32, packet + 66

        size_t tcp_header_len = tcp->th_off * 4;
        size_t ip_len = ntohs(ip_header->ip_len);
        
        size_t tcp_data_len = ip_len - ip_header_len - tcp_header_len;
        tcp_data_len = tcp_data_len > 16 ? 16 : tcp_data_len;
        PrintData(data, tcp_data_len);
      }
    }
    else if(ip_version == 6)
    {
      struct ip6_hdr *ip6 = (struct ip6_hdr*)ip_header;
      PrintIpv6(ip6);
    }
  }
  
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  while (true) {
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    if (res == 0) continue;
    if (res == -1 || res == -2) break;
    PrintPacket(packet);
    printf("%u bytes captured\n\n", header->caplen);
  }

  pcap_close(handle);
  return 0;
}
